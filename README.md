# Learning Rust with GitLab Duo (AI) - Web App 

[Slides with exercises](https://docs.google.com/presentation/d/1iwca13o-pNmEGLA2Y-ib69iC1VyVyeR8TasgDG5AFkE/edit)

## Prerequisites

1. Setup your IDE (VS Code)
1. Setup GitLab Duo Code Suggestions and Chat 
https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#use-code-suggestions 
https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#enable-gitlab-duo-chat 
1. Follow https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/ to install Rust

```sh
brew install rust 
```

or use the Rust installer.

```sh
# Download and print the script before running it
curl -Lvs https://sh.rustup.rs -o rustup-init.sh

# Run the Rust installer script
sh rustup-init.sh 
```

## Learning resources

1. https://www.rust-lang.org/learn
1. https://doc.rust-lang.org/rust-by-example/
1. https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/
1. https://about.gitlab.com/blog/2023/10/12/learn-advanced-rust-programming-with-a-little-help-from-ai-code-suggestions/ 

## Solutions

[solution/](solution/) provides example solutions for all exercises. Remember that AI is not predictable, the generated source code may look different.

Tip: Explore the examples in the two blog post tutorials linked above.

Ask the session host and attendees for help 🦊🤗