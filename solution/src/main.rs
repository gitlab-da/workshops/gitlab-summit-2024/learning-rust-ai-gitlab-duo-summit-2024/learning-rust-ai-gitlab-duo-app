// Create a webserver
// Print "Hello GitLab Summit! 🦊"
// Import necessary dependencies

use actix_web::{web, App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(|| async { "Hello GitLab Summit! 🦊" }))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

